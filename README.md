## Получение метрик с конечных устройств с помощью протокола SNMP

### Исполняемые файлы
- ``cmd/stress/main.go``. Программа для загрузки CPU (использует утилиту [``stress``](https://wiki.archlinux.org/title/Stress_testing#stress)).
- ``cmd/monitor/main.go``. Программа для сбора информации о загрузке CPU.

### Результат работы
![image](assets/work.jpg)
