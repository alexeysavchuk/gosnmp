// Package snowflake provides a custom snowflake generator. That custom
// snowflake implementation only uses timestamps and sequence. For more
// information on snowflake see https://en.wikipedia.org/wiki/Snowflake_ID.
package snowflake

import (
	"sync"
	"time"
)

type Generator struct {
	mu       sync.Mutex
	last     int64
	sequence int64
}

func New() *Generator {
	return &Generator{}
}

const start int64 = 1288834974657
const shift int64 = 22

func (s *Generator) Next() int64 {
	s.mu.Lock()
	defer s.mu.Unlock()

	current := time.Now().UnixMilli()
	if current == s.last {
		s.sequence++

		if s.sequence == 1<<shift-1 {
			for current == s.last {
				current = time.Now().UnixMilli()
			}
			s.sequence = 0
		}
	} else {
		s.sequence = 0
	}
	s.last = current

	return ((current - start) << shift) | s.sequence
}
