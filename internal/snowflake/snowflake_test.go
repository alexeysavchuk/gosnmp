package snowflake_test

import (
	"sync"
	"testing"

	"gitlab.com/alexeysavchuk/gosnmp/internal/snowflake"
)

func TestNext(t *testing.T) {
	s := snowflake.New()

	NumThreads := 1000
	NumFlakes := 10000

	ids := sync.Map{}

	wg := &sync.WaitGroup{}
	wg.Add(NumThreads)
	for i := 0; i < NumThreads; i++ {
		go func() {
			defer wg.Done()
			for i := 0; i < NumFlakes; i++ {
				id := s.Next()
				ids.Store(id, struct{}{})
			}
		}()
	}
	wg.Wait()

	count := 0
	ids.Range(func(_, _ interface{}) bool {
		count++
		return true
	})

	if count != NumThreads*NumFlakes {
		t.Errorf("Want %d unique flakes, got %d", NumThreads*NumFlakes, count)
	}
}
