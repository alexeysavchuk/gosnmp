package snmp

import (
	"encoding/asn1"
	"fmt"
	"strconv"
	"strings"
)

const (
	Version1 = iota
	Version2c
	Version3
)

type GetRequest struct {
	RequestID int
	Version   int
	Community string
	OID       string
}

type Response struct {
	RequestID   int
	Version     int
	Community   string
	ErrorStatus int
	Value       any
}

type Message struct {
	Version   int
	Community []byte
	Data      asn1.RawValue
}

type PDU struct {
	RequestID   int
	ErrorStatus int
	ErrorIndex  int
	VarbindList []Varbind
}

type Varbind struct {
	ObjectIdentifier asn1.ObjectIdentifier
	Value            asn1.RawValue
}

const (
	tagGetPDU      = 0
	tagResponsePDU = 2
)

func PrepareOID(OID string) (asn1.ObjectIdentifier, error) {
	var nodes []int
	for _, i := range strings.Split(OID, ".") {
		node, err := strconv.Atoi(i)
		if err != nil {
			return nil, err
		}
		nodes = append(nodes, node)
	}
	return asn1.ObjectIdentifier(nodes), nil
}

func MarshalGetRequest(req GetRequest) ([]byte, error) {
	OID, err := PrepareOID(req.OID)
	if err != nil {
		return nil, fmt.Errorf("failed to parse OID: %s", req.OID)
	}

	varbindList := []Varbind{
		{
			ObjectIdentifier: OID,
			Value:            asn1.RawValue{Tag: asn1.TagNull},
		},
	}

	data := PDU{
		RequestID:   req.RequestID,
		ErrorStatus: 0,
		ErrorIndex:  0,
		VarbindList: varbindList,
	}

	params := fmt.Sprintf("tag:%d", tagGetPDU)
	bytes, err := asn1.MarshalWithParams(data, params)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal 'Get' message PDU: %w", err)
	}

	msg := Message{
		Version:   req.Version,
		Community: []byte(req.Community),
		Data:      asn1.RawValue{FullBytes: bytes},
	}
	bytes, err = asn1.Marshal(msg)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal 'Get' message: %w", err)
	}

	return bytes, err
}

func UnmarshalResponse(data []byte) (*Response, error) {
	var msg Message
	_, err := asn1.Unmarshal(data, &msg)
	if err != nil {
		return nil, err
	}

	var pdu PDU
	params := fmt.Sprintf("tag:%d", tagResponsePDU)
	_, err = asn1.UnmarshalWithParams(msg.Data.FullBytes, &pdu, params)
	if err != nil {
		return nil, err
	}

	var value any
	_, err = asn1.Unmarshal(pdu.VarbindList[0].Value.FullBytes, &value)
	if err != nil {
		return nil, err
	}

	return &Response{
		RequestID:   pdu.RequestID,
		Version:     msg.Version,
		Community:   string(msg.Community),
		Value:       value,
		ErrorStatus: pdu.ErrorStatus,
	}, nil
}
