package snmp

import (
	"fmt"
	"time"

	"gitlab.com/alexeysavchuk/connpool"
)

type Client struct {
	Timeout time.Duration
	Pool    *connpool.ConnPool
}

func (c *Client) Get(req GetRequest) (resp *Response, err error) {
	bytes, err := MarshalGetRequest(req)
	if err != nil {
		return nil, fmt.Errorf("internal error: %w", err)
	}

	bytes, err = c.Do(bytes)
	if err != nil {
		return nil, fmt.Errorf("failed to send request: %w", err)
	}

	resp, err = UnmarshalResponse(bytes)
	if err != nil {
		return nil, fmt.Errorf("internal error: %w", err)
	}

	return resp, nil
}

func (c *Client) Do(bytes []byte) ([]byte, error) {
	var err error

	conn, err := c.Pool.Dial("udp", "127.0.0.1:161")
	if err != nil {
		return nil, err
	}
	defer c.Pool.Release(conn)

	done := make(chan struct{})
	defer close(done)

	go func() {
		n := len(bytes)
		for n > 0 {
			n, err = conn.Write(bytes)
			bytes = bytes[n:]
		}

		bytes = make([]byte, 4096)
		n, err = conn.Read(bytes)
		bytes = bytes[:n]

		done <- struct{}{}
	}()

	select {
	case <-time.After(c.Timeout):
		return nil, fmt.Errorf("timeout")
	case <-done:
		if err != nil {
			return nil, err
		}
	}

	return bytes, nil
}

var DefaultClient = &Client{
	Timeout: 30 * time.Second,
	Pool: connpool.NewConnPool(
		connpool.DefaultDialer,
		60*time.Second,
		10,
	),
}

func Get(req GetRequest) (*Response, error) {
	return DefaultClient.Get(req)
}
