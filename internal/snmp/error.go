package snmp

const (
	StatusNoError = iota
	StatusTooBig
	StatusNoSuchName
	StatusBadValue
	StatusReadOnly
	StatusGeneralError
	StatusNoAccess
	StatusWrongType
	StatusWrongLength
	StatusWrongEncoding
	StatusWrongValue
	StatusNoCreation
	StatusInconsistentValue
	StatusResourceUnavailable
	StatusCommitFailed
	StatusUndoFailed
	StatusAuthorizationError
	StatusNotWritable
	StatusInconsistentName
)
