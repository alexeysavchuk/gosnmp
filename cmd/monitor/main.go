package main

import (
	"fmt"
	"os"
	"time"

	"gitlab.com/alexeysavchuk/gosnmp/internal/snmp"
	"gitlab.com/alexeysavchuk/gosnmp/internal/snowflake"
)

func main() {
	snowflakeGenerator := snowflake.New()
	CPULoad := "1.3.6.1.4.1.2021.10.1.3.1"

	ticker := time.NewTicker(10 * time.Second)

	for range ticker.C {
		request := snmp.GetRequest{
			RequestID: int(snowflakeGenerator.Next()),
			Version:   snmp.Version2c,
			Community: "public",
			OID:       CPULoad,
		}

		resp, err := snmp.Get(request)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to make request: %s", err)
			os.Exit(1)
		}

		value, ok := resp.Value.([]byte)
		if !ok {
			fmt.Fprintf(os.Stderr, "Failed to extract value\n")
			os.Exit(1)
		}

		fmt.Printf("%s | CPU load: %s\n", time.Now().Format("15:04:05"), string(value))
	}
}
