package main

import (
	"context"
	"fmt"
	"os"
	"os/exec"
	"runtime"
	"time"
)

func main() {
	ctx := context.Background()
	ctx, _ = context.WithTimeout(ctx, 10*time.Second)

	cmd := exec.CommandContext(ctx, "stress", "--help")
	err := cmd.Run()
	if err != nil {
		fmt.Println(err)
		fmt.Fprintln(
			os.Stderr,
			"Can't find 'stress' command. Is it installed?",
		)
		os.Exit(1)
	}

	minCPU := 1
	maxCPU := max(1, runtime.NumCPU()/2)

	fmt.Printf("Min # of CPUs to use: %d\n", minCPU)
	fmt.Printf("Max # of CPUs to use: %d\n", maxCPU)

	ticker := time.NewTicker(10 * time.Second)

	cpu := minCPU
	timeout := 5 * time.Second
	for range ticker.C {
		fmt.Printf(
			"%s | run stress with %d CPUs\n",
			time.Now().Format("15:04:05"), cpu,
		)

		err := stress(cpu, timeout)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		cpu++
		if cpu > maxCPU {
			cpu = minCPU
		}
	}
}

func stress(cpu int, timeout time.Duration) error {
	if cpu < 0 || timeout < 0 {
		panic("Try to run stress with invalid parameters")
	}

	ctx := context.Background()
	ctx, _ = context.WithTimeout(ctx, 2*timeout)

	cmd := exec.CommandContext(ctx,
		"stress",
		"--cpu",
		fmt.Sprintf("%d", cpu),
		"--timeout",
		fmt.Sprintf("%d", timeout/time.Second),
		"--quiet",
	)
	err := cmd.Run()
	if err != nil {
		return fmt.Errorf("Failed to run stress: %w", err)
	}
	return nil
}
